﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        const int wielkosc = 20;

        static void Main(string[] args)
        {
            int[,] Tablica = new int[wielkosc, wielkosc];
            int[,] niewiem = new int[wielkosc, wielkosc];
            int licznik = 0;

            // Warunki początkowe
            Tablica[5, 5] = 1;

            Tablica[4, 5] = 1;
            Tablica[4, 6] = 1;
            Tablica[5, 6] = 1;
            Tablica[5, 8] = 1;
            Tablica[4, 7] = 1;

            // Warunki brzegowe
            for (int i = 0; i < wielkosc; i++)
            {
                Tablica[0, i] = 8;
                Tablica[wielkosc - 1, i] = 8;
                Tablica[i, 0] = 8;
                Tablica[i, wielkosc - 1] = 8;

            }
            while (true)
            {
                for (int i = 1; i < wielkosc - 1; i++)
                {
                    for (int j = 1; j < wielkosc-1; j++)
                    {
                        licznik = 0;

                        if (Tablica[i - 1, j + 1] == 1)
                            licznik++;
                        if (Tablica[i, j + 1] == 1)
                            licznik++;
                        if (Tablica[i + 1, j + 1] == 1)
                            licznik++;
                        if (Tablica[i - 1, j] == 1)
                            licznik++;
                        if (Tablica[i + 1, j] == 1)
                            licznik++;
                        if (Tablica[i - 1, j - 1] == 1)
                            licznik++;
                        if (Tablica[i, j - 1] == 1)
                            licznik++;
                        if (Tablica[i + 1, j - 1] == 1)
                            licznik++;

                        if (Tablica[i, j] == 1)
                        {
                            if (licznik == 2 || licznik == 3)
                                niewiem[i, j] = 1;
                            if (licznik < 2 || licznik > 3)
                                niewiem[i, j] = 0;
                        }
                        if (Tablica[i, j] == 0)
                        {
                            if (licznik == 3)
                                niewiem[i, j] = 1;
                        }


                    }

                }

                for (int i = 1; i < wielkosc - 1; i++)
                {
                    for (int j = 1; j < wielkosc - 1; j++)
                    {
                        Tablica[i, j] = niewiem[i, j];
                    }
                }

                Console.Clear();
                for (int i = 0; i < wielkosc; i++)
                {
                    for (int j = 0; j < wielkosc; j++)
                    {
                        Console.Write(Tablica[i, j]);
                        if (j % wielkosc + 1 == 20)
                            Console.WriteLine();
                    }
                }
                Console.ReadKey();
            }
        }
    }
}